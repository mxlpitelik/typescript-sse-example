import * as express from "express";
import { DefaultRolesEnum, getStore } from "@/sse-clients";

export const sseRouter = (() => {
  const sse = getStore();
  const router = express.Router();

  router.get("/guest", (req, res) => {
    req.socket.setTimeout(Number.MAX_VALUE);
    res.writeHead(200, {
      "Content-Type": "text/event-stream", // <- Important headers
      "Cache-Control": "no-cache",
      Connection: "keep-alive"
    });
    res.write("\n");

    sse.addClient(req, res, DefaultRolesEnum.guest);
  });

  router.get("/user", (req, res) => {
    req.socket.setTimeout(Number.MAX_VALUE);
    res.writeHead(200, {
      "Content-Type": "text/event-stream", // <- Important headers
      "Cache-Control": "no-cache",
      Connection: "keep-alive"
    });
    res.write("\n");

    sse.addClient(req, res, DefaultRolesEnum.user);
  });

  router.get("/admin", (req, res) => {
    req.socket.setTimeout(Number.MAX_VALUE);
    res.writeHead(200, {
      "Content-Type": "text/event-stream", // <- Important headers
      "Cache-Control": "no-cache",
      Connection: "keep-alive"
    });
    res.write("\n");

    sse.addClient(req, res, DefaultRolesEnum.admin);
  });

  return router;
})();
