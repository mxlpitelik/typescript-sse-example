import { DefaultRolesEnum, waitStore, SseEventsEnum } from "@/sse-clients";

export const someLogic = async () => {
  // get store object (or create if didNotExist)
  const sse = await waitStore();

  // send to all server time
  setInterval(() => {
    const msg = Date().toString();
    // sse.emitAll(msg);
    sse.emitRole(msg, DefaultRolesEnum.guest);
  }, 1000);

  // send to users group count of users online (if changed)
  let usersOnline = -1;
  sse.on(SseEventsEnum.connect, () => {
    const count = sse.count()._total;
    if (usersOnline !== sse.count()._total) {
      usersOnline = count;
      sse.emitRole(count.toString(), DefaultRolesEnum.user);
    }
  });
  sse.on(SseEventsEnum.disconnect, () => {
    const count = sse.count()._total;
    if (usersOnline !== sse.count()._total) {
      usersOnline = count;
      sse.emitRole(count.toString(), DefaultRolesEnum.user);
    }
  });

  // send to admin group details about connected/disconnected users while any changes
  sse.on(SseEventsEnum.connect, () => {
    sse.emitRole(JSON.stringify(sse.count()), DefaultRolesEnum.admin);
  });
  sse.on(SseEventsEnum.disconnect, () => {
    sse.emitRole(JSON.stringify(sse.count()), DefaultRolesEnum.admin);
  });
};
