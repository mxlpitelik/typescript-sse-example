import {Request, Response} from "express";

/**
 * enum with default roles of clients
 */
export enum DefaultRolesEnum {
  guest = "guest",
  user = "user",
  admin = "admin"
}

/**
 * enum with list of possible events
 */
export enum SseEventsEnum {
  connect = "connect",
  disconnect = "disconnect"
}

/**
 * interface with client information
 */
export interface IClient {
  userId?: string;
  res: Response;
  req: Request;
}

/**
 * intreface with counts of clients by each role + total count
 */
export interface IClientsCount {
  _total: number;
  [role: string]: number;
}

/**
 * event subscribe callback function type
 */
// tslint:disable-next-line:no-any
export type eventCallbackFunction = (data?: any) => any;
