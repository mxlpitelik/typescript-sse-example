import { SseClientsClass } from "@/sse-clients/sse-clients.class";
import { DefaultRolesEnum } from "@/sse-clients/sse-clients.types";

/**
 * function which return store instance while store will be created
 *
 * @param refreshIntervalMs - optional field for interval between requests store object (milliseconds)
 */
export const waitStore = async (refreshIntervalMs: number = 250): Promise<SseClientsClass> => {

  return new Promise((resolve) => {

    if (global["GlobalSseClientStore"]) {

      resolve(global["GlobalSseClientStore"]);

    } else {

      const tid = setInterval(() => {
        if (global["GlobalSseClientStore"]) {
          clearInterval(tid);
          resolve(global["GlobalSseClientStore"]);
        }
      }, refreshIntervalMs);

    }

  });

};

/**
 * function which create global store instance or just return if already exists
 *
 * @param roles - array of client roles, if not specified DefaultRolesEnum will be applied
 * @param defaultRole - default role, if not specified first array element will be applied
 */
export const getStore = (roles: string[] = Object.values(DefaultRolesEnum), defaultRole?: string): SseClientsClass => {
  if (!global["GlobalSseClientStore"]) {
    global["GlobalSseClientStore"] = new SseClientsClass(roles);

    if (defaultRole) {
      global["GlobalSseClientStore"].setDefaultRole(DefaultRolesEnum.guest);
    }
  }

  return global["GlobalSseClientStore"];
};
