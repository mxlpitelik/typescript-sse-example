import {eventCallbackFunction, IClient, IClientsCount, SseEventsEnum} from "./sse-clients.types";
import {Request, Response} from "express";

/**
 * Class for SSE clients operations
 */
export class SseClientsClass {
  /**
   * increment for client id generation, because clients for each service is unique - number is ok,
   * if service must be scalable and used remote storage (like redis) uuid - is better decision
   */
  private clientIdIncrement = 0;

  /**
   * list of clients grouped by roles
   */
  private clients: { [role: string]: { [clientId: string]: IClient } } = {};

  /**
   * list of users which linked to clients list
   */
  private users: { [userId: string]: IClient } = {};

  /**
   * default role of user
   */
  private defaultRole: string;

  /**
   * events callbacks registry
   */
  private events: { [event: string]: Array<() => {}> } = {};

  /**
   * constructor for store creation
   *
   * @param roles - list of roles
   * @param logger - logger instance (if not specified console.log will be used)
   * @param debugMode - if true - logs with trace and debug will be send into logger instance (default false)
   */
  constructor(
      private roles: string[],
      // tslint:disable-next-line:no-any
      private logger: any = console,
      private debugMode: boolean = false
  ) {
    if (roles.length < 1) {
      throw new Error("Roles array must contains minimum 1 element!");
    }

    roles.map((role: string) => (this.clients[role] = {}));

    this.defaultRole = roles[0];

    Object.values(SseEventsEnum).map((eventName: string) => {
      this.events[eventName] = [];
    });
  }

  /**
   * getter which generate next client id each request
   *
   * @return string with generated client id
   */
  private get clientId(): string {
    return (++this.clientIdIncrement).toString();
  }

  /**
   * method for check role
   * TODO: make it like decorator for clean code
   *
   * @param role - role for check
   *
   * @return false if role does not exist, or true if exists
   */
  private checkRole(role: string): boolean {
    return (this.roles.indexOf(role) >= 0);
  }

  /**
   * add new role to roles list
   *
   * @param role - name of role for add
   *
   * @return true if added, false if exist already in roles list
   */
  public addRole(role: string): boolean {
    if (this.roles.indexOf(role) >= 0) {
      return false;
    } else {
      this.roles.push(role);
      this.clients[role] = {};

      return true;
    }
  }

  /**
   * method for default role set, after store was created
   *
   * @param role - role for set as default
   *
   * @return true if setted as default, or false if role does not exist
   */
  public setDefaultRole(role: string): boolean {
    if (this.checkRole(role)) {
      this.defaultRole = role;
      return true;
    }

    this.logger.error({ method: "setDefaultRole", message: `Role [ ${role} ] does not exist and can't be set as default` });
    return false;
  }

  /**
   * method for add new client for sse streaming
   *
   * @param req - request stream from express (used only for disconnect event triggering)
   * @param res - response stream from express (used for message sent)
   * @param role - role of client
   * @param userId - user identifier in your DB
   *
   * @return client object pointer
   */
  public async addClient(
      req: Request,
      res: Response,
      role: string = this.defaultRole,
      userId?: string
  ): Promise<IClient> {
    if (!this.checkRole(role)) {
      this.logger.error({ method: "add", message: `Role [ ${role} ] does not exist!` });
      throw new Error(`Role [ ${role} ] does not exist, while adding user!`);
    }

    const clientId = this.clientId;

    this.clients[role][clientId] = {
      userId,
      res,
      req
    };

    if (userId) {
      this.users[userId] = this.clients[role][clientId];
    }

    req.on("close", () => {
      this.removeClient(role, clientId);
      this.registerEvent(SseEventsEnum.disconnect);

      if (this.debugMode) {
        this.logger.debug({method: "add", message: this.count(), event: "disconnect"});
      }
    });

    this.registerEvent(SseEventsEnum.connect);

    if (this.debugMode) {
      this.logger.debug({method: "add", message: this.count(), event: "connect"});
    }

    return this.clients[role][clientId];
  }

  /**
   * method for subscription on SseClientsClass events
   *
   * @param eventName - enum of event (used SseEventsEnum)
   * @param callback - callback for call while event happened
   */
  public on(eventName: SseEventsEnum, callback: eventCallbackFunction): void {
    this.events[SseEventsEnum[eventName]].push(callback);
  }

  /**
   * method for events registration (calling) from internal functions
   *
   * @param eventName - name of event (used SseEventsEnum)
   * @param data - optional parameter, if event must send some data
   */
  private registerEvent(
      eventName: SseEventsEnum,
      // tslint:disable-next-line:no-any
      data?: any
  ): void {
    this.events[SseEventsEnum[eventName]].map((func: eventCallbackFunction) => {
      // used for unblocking cycle on event triggering
      setImmediate(() => func(data));
    });
  }

  /**
   * method for removing client from store (from subscribers)
   *
   * @param role - role of client
   * @param id - id of client
   *
   * @return true - if client exists and removed, or false - if client did not exist
   */
  public async removeClient(role: string, id: string): Promise<boolean> {
    if (this.clients[role] && this.clients[role][id]) {
      if (this.clients[role][id].userId) {
        delete this.users[this.clients[role][id].userId as string];
      }

      delete this.clients[role][id];

      return true;
    } else {
      this.logger.warn({ method: "remove", message: "cant find client, maybe client removed already" });
      return false;
    }
  }

  /**
   * @return count of clients (total and by each role)
   */
  public count(): IClientsCount {
    const count: IClientsCount = { _total: 0 };

    Object.keys(this.clients).map((role) => {
      count[role] = Object.keys(this.clients[role]).length;
      count._total += count[role];
    });

    return count;
  }

  /**
   * method for find client in each role by id and message sending (slow method)
   *
   * @param clientId - id of client
   * @param msg - message for sending
   *
   * @return false if cant find any clients with active response stream
   */
  public async sendMessageById(clientId: string, msg: string): Promise<boolean> {
    for (const role in this.clients) {
      if (this.clients[role][clientId]) {
        if (this.clients[role][clientId].res) {
          return await this.sendMessage(clientId, role, msg);
        } else {
          this.logger.error({ method: "sendMessageById", message: `response stream of client [ ${clientId} ] is missing!` });
        }
      }
    }

    return false;
  }

  /**
   * method for find client by role+id and message sending (fast method)
   * @param msg - message for sent
   * @param role - role of client
   * @param clientId - id of client
   *
   * @return false if role does not exist or client does not exist/did not have active response stream
   */
  public async sendMessage(msg: string, role: string, clientId: string): Promise<boolean> {
    if (!this.checkRole(role)) {
      this.logger.error({ method: "sendMessage", message: `Role [ ${role} ] does not exist!` });
      return false;
    }

    if (this.clients[role] && this.clients[role][clientId] && this.clients[role][clientId].res) {
      this.clients[role][clientId].res.write(`data: ${msg} \n\n`); // <- Push a message to a single attached client
      return true;
    } else {
      this.logger.error({ method: "sendMessage", message: "cant find client or response stream is missing!" });
    }

    return false;
  }

  /**
   * method for send message for each client with some role
   * @param msg - message for sent
   * @param role - role of client
   *
   * @return false if role does not exist
   */
  public emitRole(msg: string, role: string): boolean {
    if (!this.checkRole(role)) {
      this.logger.error({ method: "emitRole", message: `Role [ ${role} ] does not exist!` });
      return false;
    }

    for (const id in this.clients[role]) {
      if (id) {
        this.sendMessage(msg, role, id);
      }
    }

    return true;
  }

  /**
   * method for send message for each client with some roles (you can specify many roles for emit)
   * coverage for emitRole() method
   *
   * @param msg - message for sent
   * @param rolesArr - roles list of clients
   */
  public emitRoles(msg: string, rolesArr: string[]): void {
    for (const key in rolesArr) {
      if (key) {
        if (!this.checkRole(rolesArr[key])) {
          this.logger.error({method: "emitRoles", message: `Role [ ${rolesArr[key]} ] does not exist!`});
          continue;
        }

        this.emitRole(msg, rolesArr[key]);
      }
    }
  }

  /**
   * method for send message for each client in each role
   * coverage for emitRole() method
   *
   * @param msg - message for sent
   */
  public emitAll(msg: string): void {
    for (const role in this.clients) {
      if (role) {
        this.emitRole(msg, role);
      }
    }
  }
}
