import express from "express";
import { sseRouter } from "./sse-router";
import { someLogic } from "./some-logic";

const app = express();

app.use(express.static("public"));
app.use("/events", sseRouter);

app.listen(process.env.PORT || 8080);

someLogic();
